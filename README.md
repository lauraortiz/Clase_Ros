*Documentación OpenCV*
================================

**-EXPLICACION GENERAL:**

A continuacion, se presentara el codigo desarrollado con el fin de detectar y resaltar cuadrados y rectangulos presentes en una imagen tomada en tiempo real mediante una camara. El codigo consiste en la aplicacion de ciertos filtros necesarios para procesar la imagen mas facilmente tales como conversion a escala de grises, aplicacion de un umbral, entre otros. Finalmente, se detectan los cuadrados o rectangulos y se halla su area, para posteriormente mediante una ecuacion conseguida experimentalmente, hallar la distancia entre la camara y el objeto de interes. 


-**NODOS**

- usb_cam_node 
- opemcv_threshold


**1.) INSTALACIÓN**

**1.1) Ejecutar el siguiente comando desde el terminal:**

sudo apt-get install build-essential libgtk2.0-dev libjpeg-dev libtiff4-dev libjasper-dev libopenexr-dev cmake python-dev python-numpy python-tk libtbb-dev libeigen3-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev libqt4-dev libqt4-opengl-dev sphinx-common texlive-latex-extra libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant libvtk5-qt4-dev

**1.2) Descargar archivo .zip y descomprimirlo en home**

 - cd ~
 - wget http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.9/opencv-2.4.9.zip
 - unzip opencv-2.4.9.zip

**1.3) Instalar dependencias de ROS**

 - Camara:  sudo apt-get install ros-kinetic-usb-cam 		

 - Vision Opencv Package:  sudo apt-get install ros-kinetic-vision-opencv
 

**1.4) Agregar dependencias al archivo CMakeLists.txt:**


		find_package(OpenCV)
   		include_directories(${OpenCV_INCLUDE_DIRS})
   		target_link_libraries(my_awesome_library ${OpenCV_LIBRARIES})


**2.) CODIGO DESARROLLADO**


**2.1) Librerias Necesarias**

```
 #include <ros/ros.h> : Permite el uso de las funciones de ROS.
 #include <image_transport/image_transport.h> : Permite la transformacion de la imagen leida para su posterior tratamiento. 
 #include <cv_bridge/cv_bridge.h> Permite la transformacion de la imagen leida para su posterior tratamiento. 
 #include <sensor_msgs/image_encodings.h>
 #include <opencv2/imgproc/imgproc.hpp> Libreria procesamiento de imagenes.
 #include <opencv2/highgui/highgui.hpp> Libreria procesamiento de imagenes.
 #include "opencv2/core.hpp" Libreria procesamiento de imagenes.
 #include "opencv2/imgcodecs.hpp" Libreria procesamiento de imagenes.
 #include <stdio.h>
```

**2.2) Defines**

```
 #define    NODE_NAME        	"opencv_threshold"
 #define    OPENCV_OUT          "Image Out" // VENTANA IMAGEN A MOSTRAR
 #define    OPENCV_ORI          "Image Ori" // VENTANA IMAGEN A MOSTRAR
 #define    OPENCV_MID          "Image Thre" // VENTANA IMAGEN A MOSTRAR

 #define    TOPIC1_SUB__IMAGE_INPUT      "/usb_cam/image_raw" 	//TOPIC IMAGEN DE ENTRADA
 #define    TOPIC1_PUB__IMAGE_OUTPUT     "/image_converter/output_video" ///TOPIC IMAGEN DE SALIDA
```

**2.3) Clase Principal**

- Nombre: ImageConverter
- Metodo Constructor:

```
  ImageConverter() : it_(nh_)
  	{
   // Topics declaration
       	topic1_sub__image_input = it_.subscribe(TOPIC1_SUB__IMAGE_INPUT, 1, &ImageConverter::imageCb, this);  //IMAGEN DE ENTRADA, SE LLAMARA LA FUNCION "imageCb" cada vez que llegue un mensaje al topic.
   	    topic1_pub__image_output = it_.advertise(TOPIC1_PUB__IMAGE_OUTPUT, 1); // IMAGEN DE SALIDA
    
   // Crear la ventana en la que apareceran las imagenes de salida
    	cv::namedWindow(OPENCV_OUT);
        cv::namedWindow(OPENCV_ORI);
	    cv::namedWindow(OPENCV_MID);
  	}
```  	

- Metodo Destructor:
```
  ImageConverter()
  	{
	    // close the GUI Window
    	    cv::destroyWindow(OPENCV_OUT);
	    cv::destroyWindow(OPENCV_ORI);
	    cv::destroyWindow(OPENCV_MID);
  	}
```  	

- imageCb (Funcion suscrita al topic de la imagen de entrada)

```
// Convert ROS image (Topic) to OpenCV image (Ptr)
cv_bridge::CvImagePtr cv_OriginalImage_ptr; // copy the Topic image from ROS to CvImage from OpenCV
try
{
cv_OriginalImage_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8); // possible constants: "mono8", "bgr8", "bgra8", "rgb8", "rgba8", "mono16"... Option when use "cv_bridge::CvImagePtr" data type
}
```

- Procesamiento de Imagenes 
	  
1.) Se crea un objeto del tipo "Mat", el cual es una matriz en la cual se almaceranan los valores de cada pixel de la imagen original. 

```
 cv::Mat cvMat_OriginalImage_ptr = cv_OriginalImage_ptr->image;
```

2.) Se convierte la imagen a escala de grises.    
	    
	    
```
 cv::Mat cvMat_GrayImage_ptr;
 cv::cvtColor(cvMat_OriginalImage_ptr, cvMat_GrayImage_ptr, CV_BGR2GRAY); 
```
	  
3.) Se aplica un umbral con el fin de ignorar ciertos colores y resaltar otros	    
```
cv::Mat cvMat_ThresholdImage_ptr;
threshold(cvMat_GrayImage_ptr, cvMat_ThresholdImage_ptr, threshold_value, max_BINARY_value,threshold_type );
```
	    
4.) Se detectaran las aristas y esquinas de la imagen:

```
cv::Mat detected_edges;
Canny(cvMat_ThresholdImage_ptr,detected_edges,100,200,3);
```


5.) Se encontraran los contornos de la imagen: 
	 
```
std::vector<std::vector<cv::Point> > contours;
std::vector<cv::Vec4i> hierarchy;
findContours(detected_edges,contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0)); 
```

6.) Se dibujaran los cuadros encontrados anteriormente por medio de la funcion "drawSquares"

```
drawSquares(cvMat_OriginalImage_ptr, squares);
squares.clear();
```

7.) Funcion de drawSquares:

```
 static void drawSquares( cv::Mat& image, const std::vector<std::vector<cv::Point> >& squares )
{
   float area;
   float pos;
   float distancia;

    
    for( size_t i = 0; i < squares.size(); i++ )
    {

        const cv::Point* p = &squares[0][0];
        int n = (int)squares[0].size();
        polylines(image, &p, &n, 1, true, cv::Scalar(0,255,0), 3, cv::LINE_AA);
	
 	area = pow(abs(p[1].x-p[2].x),2);
	if (area==0)
	{
	 area = pow(abs(p[1].y-p[2].y),2);
	}

	 distancia= (-5.33*area)/1000.0+45.45;
	
    }
	area=area+area;
	if (cont==3)
	   {
	    area=area/10;
	    ROS_INFO("    a=%f", distancia);
	    area=0;	
	    cont=0;
	   }
 }
```



	  
        

*Guia para ODOMETRIA*
================================

pre-requisitos:
-----------------------
sudo apt-get install ros-kinetic-turtlebot-bringup \
ros-kinetic-turtlebot-create-desktop ros-kinetic-openni-* \
ros-kinetic-openni2-* ros-kinetic-freenect-* ros-kinetic-usb-cam \
ros-kinetic-laser-* ros-kinetic-hokuyo-node \
ros-kinetic-audio-common gstreamer0.10-pocketsphinx \
ros-kinetic-pocketsphinx ros-kinetic-slam-gmapping \
ros-kinetic-joystick-drivers python-rosinstall \
ros-kinetic-orocos-kdl ros-kinetic-python-orocos-kdl \
python-setuptools ros-kinetic-dynamixel-motor-* \
libopencv-dev python-opencv ros-kinetic-vision-opencv \
ros-kinetic-depthimage-to-laserscan ros-kinetic-arbotix-* \
ros-kinetic-turtlebot-teleop ros-kinetic-move-base \
ros-kinetic-map-server ros-kinetic-fake-localization \
ros-kinetic-amcl git subversion mercurial

Nodos:
-----------------------
- rbx1_bringup/odom_ekf.py
- rbx1_nav/calibrate_angular.py
- rbx1_nav/calibrate_linear.py

Launch:
-----------------------
- odom_ekf.launch
- turtlebot_minimal_create.launch
- teleop_teclado.launch
- move_base.launch 

Paso a paso
---------------------
1)  instalar rbx1

```
 $ cd ~/catkin_ws/src
 $ git clone https://github.com/pirobot/rbx1.git
 $ cd rbx1
 $ git checkout kinetic-devel
 $ cd ~/catkin_ws
 $ catkin_make
 $ source ~/catkin_ws/devel/setup.bash
```
2)  terminal 1 abrir roscore

```
$ roscore
```

3) terminal 2 correr teleoperacion

4) terminal 3 correr launch de odometria

```
$ roslaunch rbx1_bringup odom_ekf.launch
```

5) terminal 4 abrrir mapa generado en mapeo con kinet

```
$ roslaunch (package_guardado) (nombre_del_mapa).launch 
```

6) terminal 5 abrir mapa con rviz

```
$ rosrun rviz rviz -d `rospack find rbx1_nav`/nav_ekf.rviz
```

7) si se quiere observar los datos obtenidos por la odometria en el terminal

```
$ rostopic echo \odom\pose\pose\position
```

8) para observar el arbol de eslabones de el proyecto

```
$ rosrun rqt_tf_tree rqt_tf_tree
```


*Guia para GSCAM*
================================

Dependencias:
-----------------------
- gstreamer1.0-tools 
- libgstreamer1.0-dev
- libgstreamer-plugins-base1.0-dev 
- libgstreamer-plugins-good1.0-dev
- gstreamer0.10-plugins-good

Nodos:
-----------------------
- gscam/gscam

Topics:
-----------------------
- image_raw [msg: sensor_msgs/Image]

Paso a paso
-----------------------
1. Conectar la videocamara al puerto y activar los permisos. Generalmente reconoce como video0

```
$ cd /dev/
$ ls -l
$sudo chmod 666 /dev/video0
```

2. Instalar dependencias de gscam

```
$ sudo apt-get install gstreamer1.0-tools 
$ sudo apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-good1.0-dev

```

3. Instalar package image_view (dependiendo de la versión de ROS)

```
$ sudo apt-get install ros-kinetic-image-view
```

4. Instalar package gscam

Existen dos alternativas para instalar el paquetede gscam, la primera es mediante el comando que dispone por defecto ros para la
instalacion dentro del workspace interno, y la segunda es mediante la clonacion y compilacion de dicho paquete dentro de un workspace 
creado manulamente mediante el comando catkin_make.

4.1 Metodo de instlacion por comano de ros:
```
$ sudo apt-get install ros-kinetic-gscam
```

4.2 Metodo de instalacion mediante clonacion en github, utilizando un workspace personal

```
cd catkin_ws
mkdir -p video_ws/src
cd video_ws
catkin_make
```
> Ahora se debe clonar el archivo desde github

```
$ cd src
$ git clone https://github.com/ros-drivers/gscam.git
$ cd ..
$ catkin_make
```
5. Instalar plugins adicionales

```
$ sudo apt-get install gstreamer0.10-plugins-good
```



Funcionamiento
------------------------
*Abrir 3 sesiones de terminal. En el primero se ejecuta roscore, en el segundo uno de los nodos de gscam y en el tercero uno de los nodos de image_view*

> En el primer terminal:

```
roscore
```

> En el segundo terminal:

```
$ roscd gscam
$ export GSCAM_CONFIG="v4l2src device=/dev/video0 ! video/x-raw-rgb,framerate=30/1 ! ffmpegcolorspace"

```

> En caso de que la cámara que se vaya a utilizar no esté como video0, ésta instrucción debe modificarse con el nombre correspondiente

> La cámara se enciende de la siguiente manera:

```
 $ rosrun gscam gscam
```

> Finalmente para mostrar la imagen en un tercer terminal:

```
$ rosrun image_view image_view image:=/camera/image_raw
```

Recomendaciones
-----------------------------

1. Antes de iniciar el proceso verificar que la cámara esté conectada y ubuntu la reconozca 
2. Revisar que la cámara está enviando información en el topic /camera/image_raw

```
$rostopic echo /camera/image_raw
```

3. Revisar el rqt_graph para comprobar su buen funcionamiento

```
rqt_graph
```



**Lectura laser scan Kinect**
=================================

Laser scan es una herramienta útil al momento de determinar profundidad de un entorno a partir de una cámara.

*Configuraciòn:*`

````
cd /dev/
chmod 666 tty0
`````

*Paso a paso*
1. Instalar libfrenect

````
sudo apt-get install ros-kinetic-libfreenect
````
2. Instalar freenect_launch package

````
sudo apt-get install ros-kinetic-freenect_launch
````
3. iniciar el kinect

````
roscore
````

en otro terminal

````
roslaunch freenect_launch freenect.launch
````

5. leer la imagen con image_view o rqt_image_view en el topic /camera/rgb/image_raw para una imagen de color, o en el topic /camera/depth/image_raw para una imagen de profundidad

````
rqt_image_view
````


conversiòn depth a laser scan
----------------------------------

1. Instalar el paquete depthimage_to_laserscan en un workspace

````
cd workspace
cd src
git clone https://github.com/ros-perception/depthimage_to_laserscan
cd ..
catkin_make
````

2. Una vez ejecutado freenect.launch se ejecuta el siguiente nodo

````
rosrun depthmage_to_laserscan depthimage_to_laserscan image:=/camera/depth/image_raw
````

Visualizaciòn en rviz
-----------------------

Abrir rviz

````
rviz
````

1. En la pestaña global options, seccion fixed frame, se coloca como referencia: camera_link
2. Se añade otra visualizaciòn presionando el boton Add y dentro de la carpeta rviz se selecciona la opciòn LaserScan y OK

Se muestra en el espacio de visualizaciòn los obstaculos que detecta el kinect

Se instala el paquete gmaping y rtabmap_ros por medio del comando:
````
sudo apt-get install ros-kinetic-gmaping ros-kinetic-rtabmap-ros
````
Se cierra lo que fué hecho anteriormente y se abre un nuevo terminal y se escribe:
````
rosrun depthimage_to_laserscan depthimage_to_laserscan image:=/camera/depth/image_raw
````
en otro terminal se ejecuta:
````
roslaunch freenect_launch freenect.launch depth_registration:=true
````
en otro terminal se ejecuta:
````
rosrun gmaping slam_gmaping scan:=base_scan
````
en otro terminal:
````
roslaunch rtabmap_ros rgbd_maping.launch rbgd_odometry:=true
````
Se abrirá una pantalla con la imagen de la cámara, identificando objetos por medio de puntos.
Abrir rviz
````
rviz
````

1. En la pestaña global options, sección fixed frame, se reemplaza el topic por map
2. Se añade una nueva visualización llamada map y OK

A continuación se mostrará el mapa de profundidad


*Teleoperación turtleBot2*
=========================================================
/Package ROS : rbx1 (Meta package)

/Packages incluidos = 
		rbx1
		rbx1_apps
		rbx1_bringup
		rbx1_description
		rbx1_dinamixels
		rbx1_nav
		rbx1_speech
		rbx1_vision
	
/Dependencias

* python-rosinstall (permite compilar códigos python)

/INSTALACION

1. Descargar el Package rbx1 mediante clonacion en github utilizando la carpeta workspace donde se guardan todos los archivos del robot

        $ cd <catkin_ws>/src
	$ git clone https://github.com/pirobot/rbx1.git
	# compilar con catkin_make

2. Instalar las dependencias requeridas para la librería

	$ sudo apt-get install python-rosinstall
	$ sudo apt-get install ros-kinetic-turtlebot  

3. Al realizar los dos pasos anteriores el resultado final deberia listar:

 $ roscd rbx1
 $ cd ..
 $ ls -F

 rbx1/ rbx1_bringup/ rbx1_dynamixels/ rbx1_nav/ rbx1_vision/
 rbx1_apps/ rbx1_description/ rbx1_experimental/ rbx1_speech/ README.md 

4. Configuracion de la teleoperacion por medio del Teclado

   a. Se crea un nuevo paquete con sus respectivas dependencias

        $ cd catkin_ws/src 
	$ catkin_create_pkg teleoperacion_teclado std_msgs rospy roscpp

   b.
	# compilar el workspace con "catkin_make"	

	c.
	# crear una nueva carpeta llamada "launch" dentro del package creado "teleoperacion_teclado"
			  
	d. 
	# Crear un nuevo launch y agregar el siguiente contenido
	# Nombre del launch: "teleoperacion_teclado.launch"
		<launch>
			<node name="Teleop_Irobot" pkg="turtlesim" type="turtle_teleop_key">
				<remap from="/turtle1/cmd_vel" to="cmd_vel"/>
			</node>	
		</launch>

//FUNCIONAMIENTO


Terminal 1: Ejecutamos el roscore

		$ roscore
	
Terminal 2: conéctatamos el robot al computador (usando una base iRobot Create) y con el siguiente launch enlazamos la coneccion

		$ roslaunch rbx1_bringup turtlebot_minimal_crate.launch 

		# Para poder enlazarse al ttyUSB debemos ir a la dirección /dev y generar los permisos necesarios
		$ cd /dev
		$ chmod 777 ttyUSB# (normalmente el usb es el 0)
		# Para evitar cambiar los permisos cada que se conecta el robot al computador
  		# se configura en rules de la siguiente forma.
		1.$ cd /dev
		2.$ ls   #encontramos el puerto al que está conectado el robot, que normalmente es ttyUSB0
		3.$ /etc/udev/rules.d/
		4.$ sudo gedit 99-puertos-seriales-usta.rules  #con este comando creamos el archivo gedit que permitirá agregar los permisos y que se mantengan cada que vez que conecte el robot al computador.
		5.# dentro de este gedit se agregará lo siguiente

		SUBSYSTEM=="tty",ATTRS{manufacturer}=="Prolific*",MODE="777",SYMLINK+="Irobot%n"

# lo anterior está divido en partes separadas por comas(,)
# la primera corresponde subsistema, que puede verse como la asignación previa al puerto al cual está conectado
# la segunda corresponde al fabricante y esa información puede ser obtenida de la siguiente forma para este caso:
  		6.$ uddevad info -a -n /dev/ttyUSB0
# en la información que se muestra aparece en la parte de manufacturer "Prolific", el asterisco(*) agregado al final
# nos dice que asocie todos los dispositivos que contengas este nombre.
# la tercera parte corresponde a los permisos que le queremos dar (en este caso 777)
# la cuarta parte le asocio un nuevo "nombre" a la conexión en este caso (Irobot) y el comando(%n) para que en caso de existir otro se le asocie este nombre también.
# guardamos y quedarán guardados los permisos para el robot.
Terminal 3: Lanzamos el launch que creamos para realizar la teleoperacion por medio del teclado 

		$ roslaunch teleoperacion_teclado teleoperacion_teclado.launch

========================================================================
Conexión Remmina
#para el proceso de conectarse con otro computador en ubuntu hay que tener conexión a internet
      	paso.1
	#si se trabaja en el virtualbox se hace la configuración de red
        #en el adaptador 1 se establece la conexión adaptador puente
	Paso.2
	#instalar remmina
	$sudo apt-get install remmina
	$sudo apt-get update
	$sudo apt-get upgrade
	Paso.3
	#se busca la dirección ip a la cual estamos conectados de la siguiente forma
	$ifconfig
	Paso.4
	#buscamos la aplicación remmina en el dashboard
	#dentro de ella esteblecemos una conexión VNC y se agrega el ip del equipo al cual nos vamos a conectar
	#se da click en conectar y listo! 
	
	# =====================================================================================================
	# Guía Para crear un mapa 3D con el Kinect de forma diferente a con Rviz, se logra con los siguientes pasos


	
	
	1. Es necesaria la biblioteca libfreenect y a su vez el paquete freenect_stack
	
	
	# si no se cuenta con el paquete freenect_stack se agrega de la siguiente forma

	
	
	#en el terminal se coloca
	
	
	
	-) sudo apt install ros-kinetic-freenect-stack
	
	
	# se conecta el Kinect, normalmente aparece conectado en el puerto ttyACM0
	
	
	#A travéz del siguiente código se ejecuta el launch incluido en el paquete anteriormente instalado, y
 

	
	-) roslaunch freenect_launch freenect.launch depth_registration:=true

	
	#Finalmente se ejecuta el siguiente comando en otro terminalpara abrir rviz


	
	-) rosrun rviz rviz

	
	#y se configura dentro del simulador de la forma de su preferencia


__________________________________________________________________________________________________________________________________________________

	
	
	2. #Para crear un mapa 3D y lograr una localización del turtlebot se lleva a cabo el siguiente proceso


	
	
	2.1. #se instala el paquete rtabmap en Ros

	
	$sudo apt install ros-kinetic-rtabmap-ros


	
	#Una vez finalizada la instalación, por conveniencia es preferible agregar una dirección en el .bashrc, que de forma directa puede ser


		
	$ echo export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/ros/kinetic/lib/x86_64-linux-gnu >> ~/.bashrc



	
	#Seguido a esto conectamos el kinect y probamos nuevamente su función


	
	$roslaunch freenect_launch freenect.launch depth_registration:=true

	
	
	$se inicia en otro terminal el paquete rtabmap, recordando que esta forma no se hace atravéz de rviz

	
	
	$roslaunch rtabmap_ros rgbd_mapping.launch rtabmap_args:="--delete_db_on_start"

	
	#donde se iniciará una interfaz que se asemeja a rviz y que también nos permite una visualización 3D

	#Para no estar ejecutando los paquetes y nodos que juntan cada launch se ejecuta el siguiente launch que se encargará de esto
	
	#primero en un terminal
	
	$ roscore

	#En otro terminal
	
	$roslaunch mmm_kinect mapping.launch 

	#Donde se puede evidenciar Que lo anteriormente ejecutado, ahora lo hace un solo launch
